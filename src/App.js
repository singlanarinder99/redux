import './App.css';
import {useState} from 'react';
import {useSelector,useDispatch} from 'react-redux';

function App() {
  const [text,setText] = useState('');
  const {todoReducer:todo, filterReducer} =useSelector(state=>state);
  console.log(todo, filterReducer);
  const dispatch = useDispatch();
  return (
    <div className="App">
      <h1>Todo list</h1>
      <input type="text" name="text" onChange={e=>{
        setText(e.target.value)
      }} value={text} />
      <button onClick={()=> {
        dispatch({
          type:'ADD',
          payload:{text}
        })
        setText('');
      }}>Add to do</button>
{
  todo.filter(item=>{
    if(filterReducer==='all'){
      return true;
    }
    if(filterReducer==='completed'){
      return !item.comp;
    }
    if(filterReducer==='incomplete'){
      return item.comp;
    }
    return false;
  }).map((item,index)=>{
    return (
    <div key={index}><p onClick={()=>{
      dispatch({
        type:'TOGGLE',
        payload:index
      })
    }}  className={item.comp?'inactive':'active'}>{item.text}</p>
    <button onClick={()=>{
      dispatch({
        type:'DELETE',
        payload:index
      })
    }}>delete</button>
    
    </div>
    )
  })
}
   


<ul className="list_type">
  <li className={filterReducer==="all"?"filter--active":"filter"} onClick={()=> {
    dispatch({
      type:'FILTER_LIST',
      payload:"all"
    })
  }} >all</li>
  <li className={filterReducer==="completed"?"filter--active":"filter"} onClick={()=> {
    dispatch({
      type:'FILTER_LIST',
      payload:"completed"
    })
  }}>completed</li>
  <li className={filterReducer==="incomplete"?"filter--active":"filter"} onClick={()=> {
    dispatch({
      type:'FILTER_LIST',
      payload:"incomplete"
    })
  }}>incomplete</li>
</ul>
    </div>
  );
}

export default App;
