const todoReducer = (state = [], action) => {
    switch(action.type){
        case 'ADD':
            return [...state,action.payload];
        case 'DELETE':
            let newState = [...state];
            newState.splice(action.payload,1);
            return newState;
        case 'TOGGLE':
            return state.map((item,index)=>{
                if(action.payload===index){
                    return {
                        ...item,
                        comp:!item.comp
                    }
                }
                return {
                    ...item,
                }
            }) 
            default:        
    }
return state;
}
export default todoReducer;