import todoReducer from './todo';
import filterReducer from './filter_list';
import {combineReducers} from 'redux';

const allReducers = combineReducers({
    todoReducer, filterReducer
});

export default allReducers;